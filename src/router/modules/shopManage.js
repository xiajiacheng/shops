// 店铺管理
import Layout from '@/layout'

export default {
  path: '/shopManage',
  component: Layout,
  name: 'shopManage',
  children: [
    {
      path: '/shopEquitySetting',
      component: () => import('@/views/shopManage/shopEquitySetting'),
      name: 'shopEquitySetting'
    },
    {
      path: '/shopList',
      component: () => import('@/views/shopManage/shopList'),
      name: 'shopList'
    }
  ]
}
