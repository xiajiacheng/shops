import request from '@/utils/request'

// 获取店铺信息兼分页
export function getOMOUser (params) {
  return request({
    url: '/OMO?',
    method: 'GET',
    params
  })
}

// 查询店铺信息
export function searchOMOUser (params) {
  return request({
    url: '/OMO',
    method: 'GET',
    params
  })
}

// 添加店铺信息
export function addOMOUser (data) {
  return request({
    url: '/OMO',
    method: 'POST',
    data
  })
}

// 删除店铺信息
export function delOMOUser (id) {
  return request({
    url: `/OMO/${id}`,
    method: 'DELETE'
  })
}

// 修改店铺信息
export function reWriteOMOUser (data) {
  return request({
    url: `/OMO/${data.id}`,
    method: 'PUT',
    data
  })
}

// 获取店铺数量
export function getOMOTotal () {
  return request({
    url: '/OMOtotal',
    method: 'GET'
  })
}
// 修改店铺数量
export function reWriteOMOTotal (data) {
  return request({
    url: `/OMOtotal/${data.id}`,
    method: 'PUT',
    data
  })
}
