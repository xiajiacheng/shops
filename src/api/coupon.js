import request from '@/utils/request'
export function getCouponList (params) {
  return request({
    method: 'get',
    url: '/couponList',
    params
  })
}
export function getCouponLists (params) {
  return request({
    method: 'get',
    url: '/couponLists',
    params
  })
}
export function updateCouponLists (data) {
  return request({
    method: 'put',
    url: `/couponLists/${data.id}`,
    data
  })
}
export function addCouponLists (data) {
  return request({
    method: 'post',
    url: '/couponLists',
    data
  })
}
export function deleteCouponLists (id) {
  return request({
    method: 'delete',
    url: `/couponLists/${id}`
  })
}
