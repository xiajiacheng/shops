import Layout from '@/layout'

export default {
  path: '/couponManagement',
  component: Layout,
  name: 'couponManagement',
  children: [
    {
      path: '/couponShop',
      component: () => import('@/views/couponManagement/couponShop'),
      name: 'couponShop'
    },
    {
      path: '/couponUniversal',
      component: () => import('@/views/couponManagement/couponUniversal'),
      name: 'couponUniversal'
    }
  ]
}
