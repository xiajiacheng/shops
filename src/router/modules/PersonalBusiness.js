// 订单管理
import Layout from '@/layout'

export default {
  path: '/PersonalBusiness',
  component: Layout,
  name: 'PersonalBusiness',
  children: [
    {
      path: '/ShareList',
      component: () => import('@/views/Businessdistrict'),
      name: 'ShareList'
    },
    {
      path: '/PersonalList',
      component: () => import('@/views/PersonalBusinessDistrict'),
      name: 'PersonalList'
    },
    {
      path: '/Consumption',
      name: 'Consumption',
      component: () => import('@/views/PersonalBusinessDistrict/components/Consumption.vue')
    }
  ]
}
