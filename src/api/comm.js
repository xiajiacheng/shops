import request from '@/utils/request.js'

// 获取信息 分页
export function getOMOUser (params) {
  return request({
    url: '/BaB?',
    method: 'GET',
    params
  })
}

// 查询信息
export function searchOMOUser (params) {
  return request({
    url: '/BaB',
    method: 'GET',
    params
  })
}

// 添加信息
export function addOMOUser (data) {
  return request({
    url: '/BaB',
    method: 'POST',
    data
  })
}

// 删除店铺信息
export function delOMOUser (id) {
  return request({
    url: `/BaB/${id}`,
    method: 'DELETE'
  })
}

// 修改 店铺信息
export function reWriteOMOUser (data) {
  return request({
    url: `/BaB/${data.id}`,
    method: 'PUT',
    data
  })
}

// 获取店铺数量
export function getOMOTotal () {
  return request({
    url: '/table',
    method: 'GET'
  })
}

// 修改店铺数量
export function reWriteOMOTotal (data) {
  return request({
    url: `/table/${data.id}`,
    method: 'PUT',
    data
  })
}
