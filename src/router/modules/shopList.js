// 店铺列表
import Layout from '@/layout'

export default {
  path: '/shopList',
  component: Layout,
  name: 'shopList',
  children: [
    {
      path: '/shopList',
      component: () => import('@/views/shopManage/shopList'),
      name: 'shopList'
    }
  ]
}
