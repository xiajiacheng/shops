import request from '@/utils/request'
export function getOrderStatListHD (params) {
  return request({
    method: 'get',
    url: '/orderStatListHD',
    params
  })
}
export function getOrderStatList (params) {
  return request({
    method: 'get',
    url: '/orderStatList',
    params
  })
}
export function getUseType (params) {
  return request({
    method: 'get',
    url: '/useType',
    params
  })
}
export function getOrderList (params) {
  return request({
    method: 'get',
    url: '/orderList',
    params
  })
}
