import request from '@/utils/request'

// 获取店铺信息兼分页
export function getShareUser (params) {
  return request({
    url: '/share?',
    method: 'GET',
    params
  })
}

// 查询店铺信息
export function searchShareUser (params) {
  return request({
    url: '/share',
    method: 'GET',
    params
  })
}

// 查询行业管理
export function searchProfession (params) {
  return request({
    url: '/profession?',
    method: 'GET',
    params
  })
}
// 删除行业管理
export function deleteProfession (id) {
  return request({
    url: `/profession/${id}`,
    method: 'delete'
  })
}
// 查询单个行业管理
export function queryProfession (id) {
  return request({
    url: `/profession/${id}`
  })
}
// 编辑单个行业管理
export function editProfession (data) {
  return request({
    url: `/profession/${data.id}`,
    method: 'put',
    data
  })
}
// 查询商品
export function searchGoods (params) {
  console.log(params)
  return request({
    url: '/Goods',
    method: 'GET',
    params
  })
}
// 删除商品管理
export function deleteGoodsn (id) {
  return request({
    url: `/Goods/${id}`,
    method: 'delete'
  })
}
// 查询数据
export function queryGoodsn (params) {
  return request({
    url: '/Goods?',
    method: 'GET',
    params
  })
}
// 编辑商品上架下架
export function editorGoodsn (data) {
  return request({
    url: `/Goods/${data.id}`,
    method: 'put',
    data
  })
}
