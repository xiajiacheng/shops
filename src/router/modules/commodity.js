import Layout from '@/layout'

export default {
  // 商品类别管理
  path: '/Commodity',
  component: Layout,
  name: 'Commodity',
  children: [
    {
      path: '/Commodity',
      component: () => import('@/views/Commodity/commodity'),
      name: 'Commodity'
    }
  ]
}
