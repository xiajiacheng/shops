import Layout from '@/layout'

export default {
  path: '/promoteList',
  component: Layout,
  name: 'promoteList',
  children: [
    {
      path: '',
      component: () => import('@/views/promoteList/promoteList'),
      name: 'promoteList'
      // hidden: true,

    }

  ]
}
