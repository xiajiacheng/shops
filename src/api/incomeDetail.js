import request from '@/utils/request.js'
// 获取提现列表
export function getIncomeDetail (params) {
  return request({
    method: 'get',
    url: '/incomedetail',
    params
  })
}
