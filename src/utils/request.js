// import axios from 'axios'
// import { Message } from 'element-ui' // MessageBox,
// import store from '@/store'
// import { getToken } from '@/utils/auth'

// // create an axios instance
// const service = axios.create({
//   baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
//   // withCredentials: true, // send cookies when cross-domain requests
//   timeout: 5000 // request timeout
// })

// // request interceptor
// service.interceptors.request.use(
//   config => {
//     // do something before request is sent

//     if (store.getters.token) {
//       // let each request carry token
//       // ['X-Token'] is a custom headers key
//       // please modify it according to the actual situation
//       config.headers['X-Token'] = getToken()
//     }
//     return config
//   },
//   error => {
//     // do something with request error
//     console.log(error) // for debug
//     return Promise.reject(error)
//   }
// )

// // response interceptor
// service.interceptors.response.use(
//   /**
//    * If you want to get http information such as headers or status
//    * Please return  response => response
//   */

//   /**
//    * Determine the request status by custom code
//    * Here is just an example
//    * You can also judge the status by HTTP Status Code
//    */
//   response => {
//     const res = response.data

//     // if the custom code is not 20000, it is judged as an error.
//     if (res.code !== 20000) {
//       Message({
//         message: res.message || 'Error',
//         type: 'error',
//         duration: 5 * 1000
//       })

//       // 50008: Illegal token; 50012: Other clients logged in; 50014: Token expired;
//       if (res.code === 50008 || res.code === 50012 || res.code === 50014) {
//         // to re-login
//         MessageBox.confirm('You have been logged out, you can cancel to stay on this page, or log in again', 'Confirm logout', {
//           confirmButtonText: 'Re-Login',
//           cancelButtonText: 'Cancel',
//           type: 'warning'
//         }).then(() => {
//           store.dispatch('user/resetToken').then(() => {
//             location.reload()
//           })
//         })
//       }
//       return Promise.reject(new Error(res.message || 'Error'))
//     } else {
//       return res
//     }
//   },
//   error => {
//     console.log('err' + error) // for debug
//     Message({
//       message: error.message,
//       type: 'error',
//       duration: 5 * 1000
//     })
//     return Promise.reject(error)
//   }
// )

// export default service

// 导入axios
import axios from 'axios'
// import store from '@/store'
// import router from '@/router'
// 导入获取时间戳文件
// import { getTimeStamp } from '@/utils/auth'
// const TimeOut = 7200
// 创建实例
const service = axios.create({
  // 如果执行 npm run dev 值为 /api 正确  （只是给开发环境配置的代理）
  // 如果执行 npm run build 值为 /prod-api 运维在上线时会配置
  // baseURL: process.env.VUE_APP_BASE_API,
  // baseURL: 'http://192.168.22.33:3333', // ZZY
  baseURL: 'http://192.168.22.72:3002', // zsq
  // baseURL: 'http://192.168.22.32:3001',
  // baseURL: 'http://192.168.22.53:3001',
  timeout: 5000 // 定义请求超时时间
})

// // 定义请求和响应拦截器
// // 为 axios 实例添加相应拦截器
// service.interceptors.request.use(config => {
//   // config是请求的配置信息
//   if (store.getters.token) {
//     // 如果有token，就注入token
//     if (IsCheckTimeOut()) {
//       store.dispatch('user/logout') // 超时主动调用登出操作
//       router.push('/login')// 跳到登录页
//       return Promise.reject(new Error('Token已超时'))
//     }
//     config.headers.Authorization = `Bearer ${store.getters.token}`
//   }
//   return config
// }, error => {
//   return Promise.reject(error)
// })

// service.interceptors.response.use(response => {
//   // 对响应数据要进行操作
//   // axios默认会把包裹一层data  解构赋值简化数据
//   const { success, message, data } = response.data
//   if (success) {
//     return data
//   } else {
//     Message.error(message)
//     return Promise.reject(new Error(message))
//   }
// }, error => {
//   if (error.response && error.response.data && error.response.data.code === 10002) {
//     store.dispatch('user/logout') // 根据服务器返回的状态判断Token失效后操作退出回到登录页
//     router.push('/login')
//   } else {
//     // 响应错误要进行的操作
//     Message.error(error.message)
//   }
//   return Promise.reject(new Error(error))
// })

// function IsCheckTimeOut () {
//   var currentTime = Date.now() // 当前时间戳
//   var timeStamp = getTimeStamp() // 缓存时间戳
//   return (currentTime - timeStamp) / 1000 > TimeOut
// }
// 向外暴露
export default service
