// 订单管理
import Layout from '@/layout'

export default {
  path: '/orderManagement',
  component: Layout,
  name: 'orderManagement',
  children: [
    {
      path: '/orderList',
      component: () => import('@/views/orderManagement/orderList'),
      name: 'orderList'
    },
    {
      path: '/orderStatistics',
      component: () => import('@/views/orderManagement/orderStatistics'),
      name: 'orderStatistics'
    }
  ]
}
