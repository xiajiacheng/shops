// 订单管理
import Layout from '@/layout'

export default {
  path: '/MaterialManagement',
  component: Layout,
  name: 'MaterialManagement',
  children: [
    {
      path: '/OfficialMaterial',
      component: () => import('@/views/OfficialMaterial'),
      name: 'OfficialMaterial'
    },
    {
      path: '/MaterialReview',
      component: () => import('@/views/MaterialReview'),
      name: 'MaterialReview'
    },
    {
      path: '/UserMaterial',
      component: () => import('@/views/UserMaterial'),
      name: 'UserMaterial'
    }
  ]
}
