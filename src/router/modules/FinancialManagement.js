import Layout from '@/layout'

export default {
  // 财务管理
  path: '/FinancialManagement',
  component: Layout,
  name: 'FinancialManagement',
  children: [
    // 支出明细
    {
      path: '/expensesDetail',
      component: () => import('@/views/FinancialManagement/expensesDetail'),
      name: 'expensesDetail'
    },
    // 收入明细
    {
      path: '/incomeDetail',
      component: () => import('@/views/FinancialManagement/incomeDetail'),
      name: 'incomeDetail'
    },
    // 提现
    {
      path: '/withdraw',
      component: () => import('@/views/FinancialManagement/withdraw'),
      name: 'withdraw'
    }
  ]
}
