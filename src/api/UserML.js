import request from '@/utils/request'

// 获取店铺信息兼分页
export function getUMOUser (params) {
  return request({
    url: '/UMO?',
    method: 'GET',
    params
  })
}

// 查询店铺信息
export function searchUMOUser (params) {
  return request({
    url: '/UMO',
    method: 'GET',
    params
  })
}

// 添加店铺信息
export function addUMOUser (data) {
  return request({
    url: '/UMO',
    method: 'POST',
    data
  })
}

// 通过ID获取店铺信息
export function getUMOUserById (id) {
  return request({
    url: `/UMO/${id}`,
    method: 'GET'
  })
}

// 删除店铺信息
export function delUMOUser (id) {
  return request({
    url: `/UMO/${id}`,
    method: 'DELETE'
  })
}

// 修改店铺信息
export function reWriteUMOUser (data) {
  return request({
    url: `/UMO/${data.id}`,
    method: 'PUT',
    data
  })
}

// 获取店铺数量
export function getUMOTotal () {
  return request({
    url: '/UMOtotal',
    method: 'GET'
  })
}

// 修改店铺数量
export function reWriteUMOTotal (data) {
  return request({
    url: `/UMOtotal/${data.id}`,
    method: 'PUT',
    data
  })
}
