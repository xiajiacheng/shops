import request from '@/utils/request.js'
// 获取提现列表
export function getWithdraw (params) {
  return request({
    method: 'get',
    url: '/withdraw',
    params
  })
}
export function handleWithdraw (id, data) {
  return request({
    method: 'PUT',
    url: `/withdraw/${id}`,
    data
  })
}
