import ImageUpload from './ImageUpload'
export default {
  install (Vue) {
    Vue.component('ImageUpload', ImageUpload) // 注册导入上传组件
  }
}
