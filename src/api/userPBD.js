import request from '@/utils/request'

// 获取用户信息兼切换分页
export function getPBDuser (params) {
  return request({
    url: '/PBD?',
    method: 'GET',
    params
  })
}

// 查询用户信息
export function searchPBDuser (params) {
  return request({
    url: '/PBD',
    method: 'GET',
    params
  })
}
