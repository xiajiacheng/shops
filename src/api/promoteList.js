import request from '@/utils/request.js'
// 获取推广列表//获取详情列表
export function getpromoteList (params) {
  return request({
    method: 'get',
    url: '/listaa',
    params
  })
}
// 查询用户id或昵称
export function searchuserId (params) {
  return request({
    method: 'get',
    url: '/listaa',
    params
  })
}
// 根据id获取店铺详情列表数据
export function getDetailList (id, params) {
  return request({
    method: 'get',
    url: `/shoplist${id}`,
    params
  })
}
