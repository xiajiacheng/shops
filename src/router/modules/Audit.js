import Layout from '@/layout'

export default {
  // 商品类别管理
  path: '/Audit',
  component: Layout,
  name: 'Audit',
  children: [
    {
      path: '/Audit',
      component: () => import('@/views/Audit/audit'),
      name: 'Audit'
    }
  ]
}
