import Layout from '@/layout'

export default {
  path: '/storeManagement',
  component: Layout,
  name: 'storeManagement',
  children: [
    {
      path: '/Management-goods',
      name: 'Management-goods',
      component: () => import('@/views/shopManage/Management-goods')
    },
    {
      path: '/Management-industry',
      name: 'Management-industry',

      component: () => import('@/views/shopManage/Management-industry')
    }
  ]
}
