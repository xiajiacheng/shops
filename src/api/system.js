import request from '@/utils/request'
export function getAccountList (params) {
  return request({
    method: 'get',
    url: '/accountList',
    params
  })
}
export function getAccountListHD (params) {
  return request({
    method: 'get',
    url: '/accountListHD',
    params
  })
}
export function addAccountList (data) {
  return request({
    method: 'post',
    url: '/accountList',
    data
  })
}
export function updateAccountList (data) {
  return request({
    method: 'put',
    url: `/accountList/${data.id}`,
    data
  })
}
export function deleteAccount (id) {
  return request({
    method: 'delete',
    url: `/accountList/${id}`
  })
}
