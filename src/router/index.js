import Vue from 'vue'
import VueRouter from 'vue-router'
import couponRouter from './modules/couponManagement' // 优惠券管理
import orderRouter from './modules/orderManagement' // 订单管理
import FinancialManagement from './modules/FinancialManagement' // 财务管理
import promoteList from './modules/promoteList' // 推广列表
import shopManage from './modules/shopManage'
import systemSetting from './modules/systemSetting'
import PersonalBusiness from './modules/PersonalBusiness' // 商圈管理
import MaterialManagement from './modules/MaterialManagement' // 素材管理
import commodity from './modules/commodity' // 商品类别管理
import Audit from './modules/Audit' // 商品类别管理
import storeManagement from './modules/storeManagement'
import Layout from '@/layout' // 主页模板
Vue.use(VueRouter)
export const routes = [
  // {
  //   path: '/404',
  //   component: () => import('@/views/404'),
  //   hidden: true
  // },

  {
    path: '/',
    component: Layout,
    redirect: '/home',
    children: [
      {
        path: 'home',
        name: 'home',
        component: () => import('@/views/home/index'),
        meta: { title: '主页' }
      }
    ]
  },
  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },
  {
    path: '/login',
    component: () => import('@/views/login'),
    hidden: true
  },
  {
    path: '/detail/:id',
    component: Layout,
    children: [
      {
        path: '',
        component: () => import('@/views/promoteList/detail.vue')
      }
    ]
  },
  {
    path: '/shopBasicInformation',
    component: Layout,
    redirect: '/shopBasicInformation',
    children: [
      {
        path: '',
        component: () => import('@/views/shopManage/shopBasicInformation.vue')
      }
    ]
  },
  {
    path: '/businessAnalysis',
    component: Layout,
    redirect: '/businessAnalysis',
    children: [
      {
        path: '',
        component: () => import('@/views/shopManage/businessAnalysis.vue')
      }
    ]
  },
  {
    path: '/Management-industry',
    component: Layout,
    redirect: '/Management-industry',
    children: [
      {
        path: '',
        component: () => import('@/views/shopManage/Management-industry')
      }
    ]
  },
  {
    path: '/addCoupon',
    component: Layout,
    redirect: '/addCoupon',
    children: [{
      path: '',
      component: () => import('@/views/couponManagement/addCoupon')
      // meta: { title: '主页' }
    }]
  }, {
    // 商品类别管理 子类别管理
    path: '/soncommodity',
    component: Layout,
    redirect: '/soncommodity',
    children: [{
      path: '',
      component: () => import('@/views/Commodity/soncommodity.vue')
    }]
  },
  {
    // 审核管理
    path: '/AuditManagement',
    component: Layout,
    redirect: '/AuditManagement',
    children: [{
      path: '',
      component: () => import('@/views/Audit/AuditManagement.vue')
    }]
  },
  couponRouter,
  orderRouter,
  FinancialManagement,
  promoteList,
  shopManage,
  systemSetting,
  couponRouter,
  orderRouter,
  storeManagement,
  PersonalBusiness, // 商圈管理
  MaterialManagement, // 素材管理
  commodity, // 商品类别管理
  Audit, // 开店审核
  MaterialManagement
  // ManagementIndustry,
  // ManagementGoods,
  // MaterialManagement,
]

const router = new VueRouter({
  routes
})

export default router
