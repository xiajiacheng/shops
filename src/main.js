import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui'
import './styles/index.css'
import 'element-ui/lib/theme-chalk/index.css'
import moment from 'moment'
import * as echarts from 'echarts'
import * as filters from '@/filters' // 引入工具类
import Component from '@/components'
// 注册全局的过滤器
Object.keys(filters).forEach(key => {
  // 注册过滤器
  Vue.filter(key, filters[key])
})
Vue.prototype.$echarts = echarts
Vue.config.productionTip = false

Vue.use(Component) // 注册自己的插件

Vue.use(ElementUI)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
// 时间过滤器
Vue.filter('formatDate', function (value) {
  if (!value) return ''
  return moment(value).format('YYYY-MM-DD')
})
Vue.filter('formatDateOrder', function (value) {
  if (!value) return ''
  return moment(value).format('YYYY-MM-DD hh:mm:ss')
})
