import Vue from 'vue'
// import dayjs from 'dayjs'
import dayjs from 'dayjs'
Vue.filter('formatDate', (value, format = 'YYYY-MM-DD') => {
  return dayjs(value).format(format)
})
