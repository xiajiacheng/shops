// 系统设置
import Layout from '@/layout'

export default {
  path: '/systemSetting',
  component: Layout,
  name: 'systemSetting',
  children: [
    {
      path: '/parametrSetting',
      component: () => import('@/views/systemSetting/parametrSetting'),
      name: 'parametrSetting'
    },
    {
      path: '/accountManagement',
      component: () => import('@/views/systemSetting/accountManagement'),
      name: 'accountManagement'
    }
  ]
}
