import request from '@/utils/request.js'
// 获取提现列表
export function getExpensesDetail (params) {
  return request({
    method: 'get',
    url: '/expensesdetail',
    params
  })
}
